using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class simplerotate : MonoBehaviour
{
    public float speed = 100;
    
    public GameObject gameobject;
    public bool isRotate = false;
    
    void Update()
    {
        if (isRotate)
        {
            gameobject.transform.Rotate(0, speed * Time.deltaTime, 0);
        }
          
    }

    public void enablerotate()
    {
            isRotate = true;
    }
    public void disablerotate()
    {
            isRotate = false;
    }
}